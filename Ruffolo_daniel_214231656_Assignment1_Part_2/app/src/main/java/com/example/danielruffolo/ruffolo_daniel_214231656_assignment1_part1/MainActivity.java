package com.example.danielruffolo.ruffolo_daniel_214231656_assignment1_part1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    //declare button variables
    Button startNewGame;
    Button aboutButton;
    Button scoreButton;
    Button exitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //reference the inended layout.xml
        setContentView(R.layout.activity_main);
        //initialise the view by id function
        //the id is how the intent knows what activity to launch
        startNewGame = (Button) findViewById(R.id.buttonNewGame);
        aboutButton = (Button) findViewById(R.id.buttonAbout);
        scoreButton = (Button) findViewById(R.id.buttonScoreList);
        exitButton = (Button) findViewById(R.id.buttonExit);


        //onclick listners alow the android button resource to execute a functionn upon clicking
        //On Click New Game Button
        startNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show LevelListDialog
                generateLevelListDialog();
            }
        });

        //On Click function (About Game Button)
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent aboutActivity = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(aboutActivity);
            }
        });

        //On Click function (ScoreList Button)
        scoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch Score List Activity
                Intent ViewScoresActivity = new Intent(MainActivity.this, ScoreActivity.class);
                startActivity(ViewScoresActivity);

            }
        });
        //On Click function (Exit Button)
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //exit system
                finish();
                System.exit(0);
            }
        });
    }

    // method for selecting a dificulty
    //this uses a alert dialog
    void generateLevelListDialog() {
        // Instantiate the Dialog.Builder
        //instantiate the constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        // reference the list in the dialog to the level array
        builder.setTitle("Difficulty").setItems(R.array.levels_array,
                new DialogInterface.OnClickListener() {
                //initialise the onclick listner to change game dificulty variables depending on selection
                    public void onClick(DialogInterface dialog, int which) {
                        //switch to game activity
                        Intent gameIntent = new Intent(MainActivity.this, GameActivity.class);
                        //change the variables depending on selection
                        int level = 0;
                        switch (which) {
                            //beginner
                            case 0:
                                level = 1;
                                break;
                            //intermediate
                            case 1:
                                level = 2;
                                break;
                            //advanced
                            case 2:
                                level = 3;
                                break;
                            //expert
                            case 3:
                                level = 4;
                                break;

                            default:
                                break;
                        }
                        gameIntent.putExtra("levelSelected", level);
                        //start activity with coosen difficulty
                        startActivity(gameIntent);

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


}









