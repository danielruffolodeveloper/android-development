package com.example.danielruffolo.ruffolo_daniel_214231656_assignment1_part1;

/**
 * Created by danielruffolo on 18/09/2016.
 */

// IMPLAMENTED PROPERTIES/OBJECT FOR PLAYER
    /*
    * this allows upon inserting data into the sqlite database to access our data easier as all data is groued within
    * the object of the player. the object is created once the user saves data to the sqlite database. they are given a ID and their game
    * session information is saved and populated in the list view.
    *
    * why? it is generally good practice to use getter/setter methods as properties, it helps make the code much more managable.
    *
    *
    *
    *
    *
    * */
public class Player {

    private int    _id;
    private String _name;
    private int    _score;

    public Player() {

    }

    public Player(int id, String name, int score) {

        this._id = id;
        this._name = name;
        this._score = score;
    }

    public Player(String name, int score) {
        this._name = name;
        this._score = score;
    }

    public void setID(int id) {
        this._id = id;
    }

    public int getID() {
        return this._id;
    }

    public void setPlayerName(String name) {
        this._name = name;
    }

    public String getPlayerName() {
        return this._name;
    }

    public void setScore(int score) {
        this._score = score;
    }

    public int getScore() {
        return this._score;
    }

}
