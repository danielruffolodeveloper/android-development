package com.example.danielruffolo.ruffolo_daniel_214231656_assignment1_part1;

/**
 * Created by danielruffolo on 26/08/2016.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends AppCompatActivity {

    //------global variables-----

    // width of the game table
    private int tableWidth;


    // height of the game table
    private int tableHeight;
    // location of the racket in Y axis
    private int racketY;
    // height and width of the racket
    private int RACKET_HEIGHT = 20;
    private int RACKET_WIDTH = 500;
    // size of the ball
    private int BALL_SIZE = 50;
    // Moving speed of the ball in y axis
    private int ySpeed = 30;
    // A random number
    Random rand = new Random();
    // Return a value with [-0.5,0.5], which is used to control the moving
    // direction of the ball
    // rand.nextDouble() generate a number between 0 and 1
    private double xyRate = rand.nextDouble() - 0.5;
    // The speed that the ball moves in the x axis
    private int xSpeed = (int) (ySpeed * xyRate * 2);
    // Location of the ball in the x, y axis
    // rand.nextInt(n) returns a pseudo-random uniformly distributed int in the
    // half-open range [0, n).
    private int ballX = rand.nextInt(200) + 20;
    private int ballY = rand.nextInt(10) + 20;
    // location of the racket in the x axis
    private int racketX = rand.nextInt(200);
    // if the game is over or not
    private boolean isLose = false;
    private long initialTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initialTime = System.currentTimeMillis();

        // Retrieve the data from the intent
        int level = getIntent().getIntExtra("levelSelected", -1);
        // Read the level selected in the MainActivity
        if (level != -1) {
            // Setting is specified
            setGameParameter(level);
        }
        final GameView gameView = new GameView(this);
        // Apply the gameView object for the activity
        setContentView(gameView);
        // Access the window manager to retrieve the dimensions window.
        WindowManager windowManager = getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        // Set the width and height of the game table to the screen size
        tableWidth = metrics.widthPixels;
        tableHeight = metrics.heightPixels;
        racketY = (int) (tableHeight*0.8);
        // Launch a new thread for game
        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 0x123) {
                    // call invalidate() method of the view class to draw the
                    // view object
                    gameView.invalidate();
                }
            }
        };

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() // A new thread for the ball
        {

            @Override
            public void run() {
                // If the ball hits the table on the left, change the
                // direction of the ball
                if (ballX <= 0 || ballX >= tableWidth - BALL_SIZE) {
                    xSpeed = -xSpeed;
                }
                // If ball hits the bottom and is outside the racket,
                // then stop the game
                if (ballY >= racketY - BALL_SIZE
                        && (ballX < racketX || ballX > racketX
                        + RACKET_WIDTH)) {
                    timer.cancel();
                    // Set the tag to be true which indicates that the
                    // game is over
                    isLose = true;
                }
                // If the ball hits the racket, then change the
                // direction of the ball
                else if (ballY <= 0
                        || (ballY >= racketY - BALL_SIZE
                        && ballX > racketX && ballX <= racketX
                        + RACKET_WIDTH)) {
                    ySpeed = -ySpeed;
                }
                // update the location of the ball
                ballY += ySpeed;
                ballX += xSpeed;
                // Send the message to handler to draw the gameView
                // object
                handler.sendEmptyMessage(0x123);
            }
        }, 0, 100);
    }

    // Create a new class extended from the View class
    class GameView extends View {
        Paint paint = new Paint();

        // Constructor
        public GameView(Context context) {
            super(context);
            setFocusable(true);
        }

        // Override the onDraw method of the View class to configure
        public void onDraw(Canvas canvas) {
            // Configure the paint which will be used to draw the view
            paint.setStyle(Paint.Style.FILL);
            paint.setAntiAlias(true);
            // If the game is over
            if (isLose) {
                long timeSpent = System.currentTimeMillis() - initialTime;
                timeSpent = (long) (timeSpent / 1000.0);
                generateAlertDialog(timeSpent);
            }
            // Otherwise
            else {
                // set the color of the ball
                paint.setColor(Color.rgb(240, 240, 80));
                canvas.drawCircle(ballX, ballY, BALL_SIZE, paint);
                // set the color of the racket
                paint.setColor(Color.rgb(80, 80, 200));
                canvas.drawRect(racketX, racketY, racketX + RACKET_WIDTH,
                        racketY + RACKET_HEIGHT, paint);
            }
        }

        void generateAlertDialog(final long timeSpent) {
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(
                    GameActivity.this);
            // 2. Chain together various setter methods to set the dialog
            // characteristics
            builder.setMessage(
                    "The time stayed in the game is " + timeSpent + "s")
                    .setTitle("You Lose!!")
                    .setPositiveButton("ResetGame",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // User clicked OK button
                                    finish();
                                    Intent intent = new Intent(getApplicationContext(), GameActivity.class);
                                    startActivity(intent);
                                }
                            })
                    .setNegativeButton("Exit",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // User cancelled the dialog
                                    generateAlertDialog2ForName(timeSpent);
                                }
                            });

            // 3. Get the AlertDialogfrom create()
            AlertDialog dialog = builder.create();

            // 4. Show dialog
            dialog.show();
        }

        void generateAlertDialog2ForName(final long timeSpent) {

            AlertDialog.Builder alert = new AlertDialog.Builder(GameActivity.this);

            alert.setTitle("Score = " + timeSpent);
            alert.setMessage("Please Input The Players name");

            // Set an EditText view to get user input
            final EditText input = new EditText(GameActivity.this);
            alert.setView(input);

            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String Player_name = input.getText().toString();

                    String strLong = Long.toString(timeSpent);

                    // Do something with value!

                    Intent GameDataIntent = new Intent(GameActivity.this, ScoreActivity.class);
                    GameDataIntent.putExtra("gamescore",strLong);
                    GameDataIntent.putExtra("playername", Player_name);
                    finish();
                    startActivity(GameDataIntent);


                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                    finish();
                }
            });

            alert.show();



        }

        // Override the onTouchEvent method in the View class
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            super.onTouchEvent(event);
            // Get the location of user's figure
            float x = event.getX();
            // Determine if the user's figure is on racket or not
            if (x > racketX && x < racketX + RACKET_WIDTH) {
                // if yes, set the location of the racket to user's figure
                // location so that the user can drag the racket
                racketX = (int) x - RACKET_WIDTH / 2;
                // draw the game view object
                invalidate();
            }
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.gameActivityExit) {
            finish();
            Intent intent = new Intent(GameActivity.this, MainActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.gameActivityReset) {
            finish();
            Intent intent = new Intent(GameActivity.this, GameActivity.class);
            startActivity(intent);


        }
        if (id == R.id.gameActivityLevel) {

           generateListDialog();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void generateListDialog() {

        // Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);

        // Specify the list in the dialog using the array
        builder.setTitle("Select Difficulty").setItems(R.array.levels_array,
                new DialogInterface.OnClickListener() {
                    // Chain together various setter methods to set the list
                    // items
                    // The index of the item selected is passed by the parameter
                    // which
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {

                            case 0:
                                setGameParameter(1);
                                break;

                            case 1:
                                setGameParameter(2);
                                break;

                            case 2:
                                setGameParameter(3);
                                break;
                            case 3:
                                setGameParameter(4);
                                break;
                            default:
                                break;
                        }
                        finish();
                        Intent gameIntent = new Intent(getApplicationContext(),GameActivity.class);
                        startActivity(gameIntent);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void setGameParameter(int level) {
        switch (level){
            case 1:
                // Beginner game setting
                RACKET_WIDTH = 500;
                BALL_SIZE = 50;
                ySpeed = 30;
                break;
            case 2:
                //intermediate
                RACKET_WIDTH = 200;
                BALL_SIZE = 45;
                ySpeed = 35;
                break;
            case 3:
                // advanced game setting
                RACKET_WIDTH = 100;
                BALL_SIZE = 40;
                ySpeed = 40;
                break;
            case 4:
                //expert game setting
                RACKET_WIDTH = 50;
                BALL_SIZE = 35;
                ySpeed = 45;
                break;

            default:
                RACKET_WIDTH = 500 ;
                BALL_SIZE = 50;
                ySpeed = 30;
        }
    }
}