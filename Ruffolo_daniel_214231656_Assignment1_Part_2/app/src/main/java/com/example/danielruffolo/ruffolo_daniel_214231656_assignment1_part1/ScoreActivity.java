package com.example.danielruffolo.ruffolo_daniel_214231656_assignment1_part1;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;


/**
 * Created by danielruffolo on 16/09/2016.
 */

public class ScoreActivity extends AppCompatActivity {


    ScoreDBHandler dbHandler; //declare our sql class as the database helper
    SimpleCursorAdapter simpleCursorAdapter; //declare our cursor adaptor used to link the sql data with the list adapter
    ListView listviewPlayers; //declare our list view

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

//link our list view with the xml layout
        listviewPlayers = (ListView) findViewById(R.id.playerList);
        dbHandler = new ScoreDBHandler(this, null, null, 1);


//call methods to begin the program
        displayPlayerScoreList();
        newPlayerScore();
        Toast.makeText(getApplicationContext(), "Scrole to view scores or press back on your phone to exit", Toast.LENGTH_LONG).show();

    }

    public void newPlayerScore() {
        try {
//i decided to wrap these all in try catches. if they ever return a null value.....it crashes the app
            //parse data from our game activity via intent  as strings
            Intent myIntent = getIntent();
            String sentPlayerScore = myIntent.getStringExtra("gamescore");
            String sentPlayerName = myIntent.getStringExtra("playername");

            //convert score back to integer
            int PlayerScore_converted_value = Integer.parseInt(sentPlayerScore.toString());

            //as this point we create the new player object with data from our game view
            Player p = new Player(sentPlayerName.toString(), Integer.parseInt(String.valueOf(PlayerScore_converted_value)));
            dbHandler.addPlayerScore(p);
            //the data handler is our class that handles all the sql


//i added toasts because user feedback is always a good thing
            Toast.makeText(getApplicationContext(), "DATA ADDED", Toast.LENGTH_LONG).show();

            displayPlayerScoreList();
            //calling the method again will update the list adapter

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "NO SCORE ADDED", Toast.LENGTH_LONG).show();
        }
    }


    private void displayPlayerScoreList() {

        try {
            //use the dbhandler to get access and call the get all scores method
            //initiaate the cursor object and tell it to get all scores
            //here again i used a try loop to stop error crash
            //if their wsnt a value inserted, it wouldnt have a value to populate and that causes error
            Cursor cursor = dbHandler.getAllScores();
            if (cursor == null) {
                Toast.makeText(getApplicationContext(), "CURSOR ERROR, PLEASE PLAY A GAME", Toast.LENGTH_LONG).show();
                return;
            }
            if (cursor.getCount() == 0) {
                Toast.makeText(getApplicationContext(), "ERROR,THEIR ARE NO SCORES CURRENTLY", Toast.LENGTH_LONG).show();
                return;
            }
            String[] columns = new String[]{
                    ScoreDBHandler.COLUMN_ID, ScoreDBHandler.COLUMN_PLAYERNAME, ScoreDBHandler.COLUMN_PLAYERSCORE
            };
            int[] boundTo = new int[]{
                    R.id.playerId,
                    R.id.playerName,
                    R.id.playerScore,

                    //this bound to is what actually links to the list view


            };
            simpleCursorAdapter = new SimpleCursorAdapter(this,
                    R.layout.player_list,
                    cursor, columns, boundTo, 0);
            listviewPlayers.setAdapter(simpleCursorAdapter);



        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "SCORE LOOP ERROR", Toast.LENGTH_LONG).show();
        }


    }
}
