package com.example.danielruffolo.ruffolo_daniel_214231656_assignment1_part1;

/**
 * Created by danielruffolo on 16/09/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by danielruffolo on 16/09/2016.
 */


public class ScoreDBHandler extends SQLiteOpenHelper {

    //specify our database version
    private static final int DATABASE_VERSION = 1;
    //initiate a game score Database with name
    private static final String DATABASE_NAME = "gamescoreDBFINAL.db";
    //name the table for our score info
    public static final String TABLE_PLAYERS = "players";
//declare colums for scores
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_PLAYERNAME = "playername";
    public static final String COLUMN_PLAYERSCORE = "playerscore";

    //declare the Database and cursor
    public ScoreDBHandler(Context context, String name,
                          SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //build the database
        String create_products_table = "CREATE TABLE " + TABLE_PLAYERS + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_PLAYERNAME + " TEXT," +
                COLUMN_PLAYERSCORE + " INTEGER )";
        db.execSQL(create_products_table);
    }

    @Override
    //if it has been created, remove
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PLAYERS);
        onCreate(db);
    }
//cursor method that steps through the sq database, retrieves data accessed by db.query
    public Cursor getAllScores() {
        SQLiteDatabase db = this.getReadableDatabase();

        //NOTE , we are sorting data here because given that the list is forever changing, we need to order by every time we request
        //the list view adaptor
        Cursor cursor = db.query(true,TABLE_PLAYERS, new String[]{COLUMN_ID, COLUMN_PLAYERNAME,
                COLUMN_PLAYERSCORE}, COLUMN_ID+" = "+COLUMN_ID, null, null, null, COLUMN_PLAYERSCORE+" DESC", null);

        if (cursor != null) {
            cursor.moveToFirst();
            return cursor;
        } else {
            return null;
        }
    }

///here we are inserting our player object data intothe db
    public void addPlayerScore(Player player) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_PLAYERNAME, player.getPlayerName());
        values.put(COLUMN_PLAYERSCORE, player.getScore());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_PLAYERS, null, values);
        db.close();
    }

}