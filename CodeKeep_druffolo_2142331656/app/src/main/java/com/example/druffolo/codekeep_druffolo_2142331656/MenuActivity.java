package com.example.druffolo.codekeep_druffolo_2142331656;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by danielruffolo on 3/10/2016.
 */
public class MenuActivity extends AppCompatActivity {

    Button addcode;
    Button viewcode;
    Button coderesources;
    Button about;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        addcode = (Button) findViewById(R.id.button_savecode);

        coderesources = (Button) findViewById(R.id.button_viewlist);
        about = (Button) findViewById(R.id.button4);

        //On Click function (About Game Button)
        addcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent listviewactivity = new Intent(MenuActivity.this, ListViewActivity.class);
                startActivity(listviewactivity);
            }
        });

        //On Click function (About Game Button)
        coderesources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent website1 = new Intent(MenuActivity.this, CodeResources.class);
                startActivity(website1);
            }
        });
        //On Click function (About Game Button)
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent website1 = new Intent(MenuActivity.this, AboutActivity.class);
                startActivity(website1);
            }
        });
    }
}






