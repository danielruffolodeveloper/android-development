package com.example.druffolo.codekeep_druffolo_2142331656;

/**
 * Created by danielruffolo on 3/10/2016.
 */

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class CodeItemActivity extends AppCompatActivity {

    //initialise variables
    //inherit serialisable object
    Code code;
    private static final int CREATE_CODEITEM = 1;
    private static final int EDIT_CODEITEM = 2;
    private int functionmode;
    private EditText codetextTitle;
    private EditText codetextContent;
    //boolean for refreshing list view
    private boolean Refresh_RequestAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set the activity xml file to the layout
        setContentView(R.layout.activity_codeitem);

        //set edit text to equal code text title
        this.codetextTitle = (EditText)this.findViewById(R.id.text_note_title);
        //set edittext to equal code text content
        this.codetextContent = (EditText)this.findViewById(R.id.text_note_content);

        //get the code from SQL DB via intent
        Intent intent = this.getIntent();
        this.code = (Code) intent.getSerializableExtra("code");

        //if the code has a value inside it
        //runn all functions to make a new list object item
        if(code!= null)  {
            this.functionmode = CREATE_CODEITEM;
        } else  {
            this.functionmode = EDIT_CODEITEM;
            this.codetextTitle.setText(code.getCodeTitle());
            this.codetextContent.setText(code.getCodeContent());
        }

    }

    //the save function

    public void buttonSaveClicked(View view)  {
        //on click, call the db helper class
        MyDBHelper db = new MyDBHelper(this);

        String code_title = this.codetextTitle.getText().toString(); //convert code text title to string
        String code_content = this.codetextContent.getText().toString();//convert content of the entry to string

        if(code_title.equals("") || code_content.equals("")) {
            //if either are blank , not just one
            //force error
            //ask to fill in all fields
            Toast.makeText(getApplicationContext(),
                    "Please Enter Content in all fields", Toast.LENGTH_LONG).show();
            return;
        }

        if(functionmode==CREATE_CODEITEM )

        //if the create code item ic called
        //set local variables to equal global variables
        {
            this.code= new Code(
                    code_title,code_content);
            //add to db
            db.addCode(code);
        } else  {
            this.code.setCodeTitle(code_title);
            this.code.setCodeContent(code_content);
            //overwrite the code list item in db
            db.updateCode(code);
        }

        this.Refresh_RequestAction = true;
        this.onBackPressed();
        //this will go back to the list item and refresh the db and list view
    }

    // cancel alert button
    public void buttonCancelClicked(View view)  {
        // go back to list view activity
        // .
        this.onBackPressed();
    }

    @Override
    public void finish() {
        // Create Intent to call the refresh list view function
        Intent data = new Intent();

        // request the list activity with db entries

        data.putExtra("RefreshAction", Refresh_RequestAction);

        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the action menu.this will add items to the action bar if it is present in activity.
        getMenuInflater().inflate(R.menu.menu_functions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.ActivityExitToMenu) {
            Intent intent = new Intent(CodeItemActivity.this, MenuActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}