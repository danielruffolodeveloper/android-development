package com.example.druffolo.codekeep_druffolo_2142331656;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

/**
 * Created by danielruffolo on 3/10/2016.
 */
public class CodeResources extends AppCompatActivity {

    Button webview1;
    Button webview2;
    Button webview3;
    Button webview4;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coderesources);

        webview1 = (Button)findViewById(R.id.resbutton1);
        webview2 = (Button)findViewById(R.id.resbutton2);
        webview3 = (Button)findViewById(R.id.resbutton3);
        webview4 = (Button)findViewById(R.id.resbutton4);




        //On Click function (About Game Button)
        webview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent website1 = new Intent(CodeResources.this, webview1.class);
                startActivity(website1);
            }
        });

        //On Click function (About Game Button)
        webview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent website2 = new Intent(CodeResources.this, webview2.class);
                startActivity(website2);
            }
        });

        //On Click function (About Game Button)
        webview3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent website3 = new Intent(CodeResources.this, webview3.class);
                startActivity(website3);
            }
        });

        //On Click function (About Game Button)
        webview4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent website4 = new Intent(CodeResources.this, webview4.class);
                startActivity(website4);
            }
        });









    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the action menu.this will add items to the action bar if it is present in activity.
        getMenuInflater().inflate(R.menu.menu_functions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.ActivityExitToMenu) {
            Intent intent = new Intent(CodeResources.this, MenuActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
