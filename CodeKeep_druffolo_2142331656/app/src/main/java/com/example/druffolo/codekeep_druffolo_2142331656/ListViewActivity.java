package com.example.druffolo.codekeep_druffolo_2142331656;

/**
 * Created by danielruffolo on 3/10/2016.
 */


import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    private ListView listView;


    private static final int VIEWCODE = 1;
    private static final int EDITCODE = 2;
    private static final int CREATECODE = 3;
    private static final int DELETECODE = 4;
    private static final int GETCODE = 1;

    private final List<Code> codeList = new ArrayList<Code>();
    private ArrayAdapter<Code> listViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customlistview);


        // associate the list view with the relevent xml file

        listView = (ListView) findViewById(R.id.listView);
        MyDBHelper db = new MyDBHelper(this);

        db.createDefaultCodesIfNeed();

        List<Code> list=  db.getAllCodes();

        this.codeList.addAll(list);

        //use the inbuilt list item adapter
        this.listViewAdapter = new ArrayAdapter<Code>
                (this, android.R.layout.simple_list_item_1,
                        android.R.id.text1, this.codeList);


        // set our selected list view to the list view adapter
        this.listView.setAdapter(this.listViewAdapter);

        // set the list view to work with context menu
        registerForContextMenu(this.listView);
    }


    //this is the on create context mnu, it works by longholfing a list view item
    //when you long hold a item , it will bring up the options below
    @Override
    public void onCreateContextMenu(ContextMenu functionmenu, View view, ContextMenu.ContextMenuInfo menuInfo)    {

        super.onCreateContextMenu(functionmenu, view, menuInfo);
        functionmenu.setHeaderTitle("SELECT A OPTION");

        //each function is associated with a number, that number requests the database method , similar to array
        functionmenu.add(0, VIEWCODE , 0, "View Code");
        functionmenu.add(0, CREATECODE , 1, "Create Code");
        functionmenu.add(0, EDITCODE , 2, "Edit Code");
        functionmenu.add(0, DELETECODE, 4, "Delete Code");
    }

    //method to initiate the get menu
    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo

                Menuinfo = (AdapterView.AdapterContextMenuInfo)
                item.getMenuInfo();

        //this gets the list position of the list entry position selected
        final Code selectedCode = (Code) this.listView.getItemAtPosition(Menuinfo.position);

        //if the menu item number = VIEWCODE , run this function
        if(item.getItemId() == VIEWCODE){
            Toast.makeText(getApplicationContext(),selectedCode.getCodeContent(),Toast.LENGTH_LONG).show();
        }

        //if the menu item number = CREATECODE , run this function
        else if(item.getItemId() == CREATECODE){
            Intent intent = new Intent(this, CodeItemActivity.class);

          //get the content of the list item via intent
            this.startActivityForResult(intent, GETCODE);
        }
        //if the menu item number = EDITCODE , run this function
        else if(item.getItemId() == EDITCODE ){
            Intent intent = new Intent(this, CodeItemActivity.class);
            intent.putExtra("code", selectedCode);

            //get the content of the list item via intent
            this.startActivityForResult(intent,GETCODE);
        }
        //if the menu item number = DELETECODE , run this function
        else if(item.getItemId() == DELETECODE){

            //allert the user before deleting
            new AlertDialog.Builder(this)
                    .setMessage(selectedCode.getCodeTitle()+". DELETE?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteCode(selectedCode);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        else {
            return false;
        }
        return true;
    }

    //method to support the delete code entry function
    private void deleteCode(Code code)  {
        MyDBHelper db = new MyDBHelper(this);
        db.deleteCode(code);
        this.codeList.remove(code);
        // Refresh to show that the code has been deleted and is no longer in the list view
        this.listViewAdapter.notifyDataSetChanged();
    }


    //
    @Override
    //we are requesting the database entries via intent
    //if the function RESULT_OK and GETCODE have been met
    //refresh the data
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == GETCODE ) {

            boolean RefreshList = data.getBooleanExtra("Refresh",true);
            if(RefreshList) {
                this.codeList.clear();
                MyDBHelper db = new MyDBHelper(this);
                List<Code> tempList=  db.getAllCodes();
                this.codeList.addAll(tempList);
                this.listViewAdapter.notifyDataSetChanged();
            }


        }


    }

}