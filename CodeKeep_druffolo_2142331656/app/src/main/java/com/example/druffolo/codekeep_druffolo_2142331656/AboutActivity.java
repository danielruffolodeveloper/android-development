//Druffolo 214231656

package com.example.druffolo.codekeep_druffolo_2142331656;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

/**
 * Created by danielruffolo on 3/10/2016.
 */
public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        VideoView myVideoVeiw = (VideoView) findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.appvideo);
        myVideoVeiw.setVideoURI(uri);
        myVideoVeiw.start();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the action menu.this will add items to the action bar if it is present in activity.
        getMenuInflater().inflate(R.menu.menu_functions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.ActivityExitToMenu) {
            Intent intent = new Intent(AboutActivity.this, MenuActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}




