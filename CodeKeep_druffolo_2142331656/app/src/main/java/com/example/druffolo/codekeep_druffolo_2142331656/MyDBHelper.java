package com.example.druffolo.codekeep_druffolo_2142331656;

/**
 * Created by danielruffolo on 3/10/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class MyDBHelper extends SQLiteOpenHelper {

    //declare variables for the database
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Code_Manager";
    public static final String TABLE_CODE = "Code";
    public static final String COLUMN_CODE_ID ="Code_Id";
    public static final String COLUMN_CODE_TITLE ="Code_Title";
    public static final String COLUMN_CODE_CONTENT = "Code_Content";
    public MyDBHelper(Context context)  {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Create table for the code entries
    @Override
    public void onCreate(SQLiteDatabase db) {

        String script = "CREATE TABLE "
                + TABLE_CODE + "("
                + COLUMN_CODE_ID
                + " INTEGER PRIMARY KEY,"
                + COLUMN_CODE_TITLE
                + " TEXT,"
                + COLUMN_CODE_CONTENT
                + " TEXT"
                + ")";

        db.execSQL(script);
        //create the able in the db
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // drop and recreate if the table does allready exist
        //this will eliminate possibility of error
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CODE);
        onCreate(db);//call method to create db again
    }

    public void createDefaultCodesIfNeed()
    //his method on creation will enter the instructions list item automatically
    {
        //see if the list view allready has entries
        //if it doesnt (count = 0 )
        int count = this.getCodesCount();

        if(count < 1 || count == 0  ) {
            Code code1 = new Code("LONG CLICK ME FOR MENU",
                    "this is how you add, edit or delete a entry");

            this.addCode(code1); //add this to the list db entries

        }
    }


    public void addCode(Code code) {

        //this method will add the code to the db

       code.getCodeTitle();
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        //create new value placement to store into db

        values.put(COLUMN_CODE_TITLE, code.getCodeTitle()); //retrieved from the method value
        values.put(COLUMN_CODE_CONTENT, code.getCodeContent());//retrieved from the method value

        // Insert into the table row
        db.insert(TABLE_CODE, null, values);

        // Closing database connection
        db.close();
    }


    public Code getCode(int id) {

        //this method will populate the list view db based on the id of rows in the db

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CODE, new String[] {
                COLUMN_CODE_ID,
                COLUMN_CODE_TITLE,
                COLUMN_CODE_CONTENT }
                , COLUMN_CODE_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        //we use the cursor to populate the list view with values from the db moving through every single db value

        Code code = new Code(
                Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return the actual data
        //each number here is calling the functions built before
        //it is easier to hadle the functions this way
        return code;
    }

    public List<Code> getAllCodes() {

        //this method will populate the list with the code enntries

        List<Code> codeList = new ArrayList<Code>();
        //we use an array list so that we can store the data in our list view

        // Select All Query that will grab all of our data entries

        String selectQuery = "SELECT  * FROM " + TABLE_CODE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // this will start the cursor
        //move it through every DB row
        //add it to a list entry
        //again we call the functions 1,2,0 that we built in the db
        //the cursor plays the most important role here

        if (cursor.moveToFirst()) {
            do {
                Code code = new Code();
                code.setCodeId(
                        Integer.parseInt(cursor.getString(0)));
                code.setCodeTitle(
                        cursor.getString(1));
                code.setCodeContent(
                        cursor.getString(2));
                codeList.add(code);
            } while (cursor.moveToNext());
        }

        return codeList;
        //all the data that the cursor wrote to the list, we return it
        //this gives us the list filled
    }

    public int getCodesCount() {

        //this method will count the total entries
        //this is used to tell the app what is in the db
        //if their is none, write that defult entry
        String countQuery = "SELECT  * FROM " + TABLE_CODE;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();

        cursor.close();
        return count;
    }


    //this method will support the edit function
    //if the entry has been edited or selected to be edit,
    // all the data in that position currently is overwritten
    public int updateCode(Code code) {
       code.getCodeTitle();

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_CODE_TITLE, code.getCodeTitle());
        values.put(COLUMN_CODE_CONTENT, code.getCodeContent());

        return db.update(TABLE_CODE, values, COLUMN_CODE_ID + " = ?",
                new String[]{String.valueOf(code.getCodeId())});
    }

    public void deleteCode(Code code) {
        code.getCodeTitle() ;

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CODE, COLUMN_CODE_ID + " = ?",
                new String[] { String.valueOf(code.getCodeId()) });
        db.close();
    }

}