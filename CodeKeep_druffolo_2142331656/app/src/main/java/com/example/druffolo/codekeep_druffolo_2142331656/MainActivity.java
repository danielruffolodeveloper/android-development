//Druffolo 214231656


package com.example.druffolo.codekeep_druffolo_2142331656;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    ImageButton keybutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        keybutton = (ImageButton) findViewById(R.id.androidkey);

        keybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //launch About Activity
                Intent MenuActivity = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(MenuActivity);
            }
        });

    }


}
