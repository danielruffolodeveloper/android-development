package com.example.druffolo.codekeep_druffolo_2142331656;

/**
 * Created by danielruffolo on 3/10/2016.
 */

//this is our serialisable object class for the code entries
//each code entry saves to the sql db with these variables
//this allows us to have multiple entries

import java.io.Serializable;
public class Code implements Serializable {

    //each has a id, title,content
    private int codeId;
    private String codeTitle;
    private String codeContent;

    //constructor for the object
    public Code()  {}

    public Code(  String codeTitle, String codeContent) {
        this.codeTitle= codeTitle;
        this.codeContent= codeContent;
    }

    public Code(int codeId, String codeTitle, String codeContent) {
        this.codeId= codeId;
        this.codeTitle= codeTitle;
        this.codeContent= codeContent;
    }

    //oncall of this method, returns codeId
    public int getCodeId() {
        return codeId;
    }

    //on return of this method, a ne code entry is assigned a id
    public void setCodeId(int codeId) {
        this.codeId = codeId;
    }
    //on return of this method, the code title is returned
    public String getCodeTitle() {
        return codeTitle;
    }

    //on return of this method, a new code entry title is set
    public void setCodeTitle(String codeTitle) {
        this.codeTitle = codeTitle;
    }


    //on return of this method, the code conent is returned to the list item
    public String getCodeContent() {
        return codeContent;
    }

    //on return of this method, a new code entry content is set
    public void setCodeContent(String codeContent) {
        this.codeContent = codeContent;
    }


    @Override
    public String toString()  {
        return this.codeTitle;
    }

}
